import { Ref, ref } from 'vue'

/**
 * 打字效果
 * @param { Object } content 打字的内容
 */
export default function useTyped(content: string): Ref<string> {
  let time: any = null
  let i:number = 0
  let typed = ref('_')
  function autoType() {
    if (typed.value.length < content.length) {
      time = setTimeout(() =>{
        typed.value = content.slice(0, i+1) + '_'
        i++
        autoType()
      }, 200)
    } else {
      clearTimeout(time)
      typed.value = content
    }
  }
  autoType()
  return typed
}