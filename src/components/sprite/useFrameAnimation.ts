import { Ref } from "vue";
import { positionInterface, spriteInterface } from "../../interface";

/**
 * 精灵图实现逐帧动画
 * @param spriteObj 精灵对象
 * @param target 精灵节点
 * @param wrap 精灵父节点 [控制精灵移动]
 * @param callback 图片加载好回调函数
 * @param moveCallback 移动到对应位置的回调函数
 */
export function useFrameAnimation(
  spriteObj: spriteInterface,
  target: Ref,
  wrap: Ref,
  callback: Function,
  moveCallback: Function
) {
  const { width, length, url, endPosition } = spriteObj;
  let index = 0;

  var img = new Image();
  img.src = url;
  img.addEventListener("load", () => {
    let time;
    (function autoLoop() {
      callback && callback();
      // 如果到达了指定的位置的话，则停止
      if (isEnd(wrap, endPosition)) {
        if (time) {
          clearTimeout(time);
          time = null;
          moveCallback && moveCallback();
          return;
        }
      }
      if (index >= length) {
        index = 0;
      }
      target.value.style.backgroundPositionX = -(width * index) + "px";
      index++;
      // 使用setTimeout, requestFrameAnimation 是60HZ进行渲染，部分设备会卡，使用setTimeout可以手动控制渲染时间
      time = setTimeout(autoLoop, 160);
    })();
  });

  // 走到了对应的位置
  function isEnd(wrap, endPosition: positionInterface) {
    let keys = Object.keys(endPosition);
    for (let key of keys) {
      if (window.getComputedStyle(wrap.value)[key] === endPosition[key]) {
        return true;
      }
    }
    return false;
  }
}
