// 剧本
export const dialogueContent = [
  {
    avatar: "/images/rpg_male.png",
    content: "二牛：嫦娥你终于肯和我约会了, 哈哈",
  },
  {
    avatar: "/images/rpg_female.png",
    content: "嫦娥：二牛对不起，我是从月宫来的，我不能和人间的你在一起!",
  },
  {
    avatar: "/images/rpg_female.png",
    content:
      "嫦娥：今天是中秋节，我只有今天这个机会可以重新回月宫",
  },
  {
    avatar: "/images/rpg_female.png",
    content:
      "嫦娥：回月宫的条件是找到真心人，让他念起咒语，我才能飞升!",
  },
  {
    avatar: "/images/rpg_female.png",
    content: "嫦娥：而你就是我的真心人，你可以帮我嘛？",
  },
  {
    avatar: "/images/rpg_male.png",
    content: "二牛：好的，我明白了! 我会帮你的.",
  },
  {
    avatar: "/images/rpg_female.png",
    content: "嫦娥：好的。 谢谢你!",
  },
];


// 男孩的精灵图
export const boyData = {
  length: 4, // 精灵图的长度
  url: "/images/boy.png", // 图片的路径
  width: 37.5, // 图片的宽度
  height: 72.5, // 图片的高度
  endPosition: {
    left: "300px",
    bottom: "154px",
  },
};

// 女孩的精灵图
export const girlData = {
  length: 7, // 精灵图的长度
  url: "/images/girl.png", // 图片的路径
  width: 37.5, // 图片的宽度
  height: 72.5, // 图片的高度
  scale: 0.2,
  endPosition: {
    bottom: "325px",
    right: "140px",
  },
};